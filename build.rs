use embed_manifest::manifest::{ExecutionLevel, HeapType, SupportedOS};
use embed_manifest::{embed_manifest, empty_manifest};

fn main() {
    let manifest = empty_manifest()
        .heap_type(HeapType::SegmentHeap)
        .requested_execution_level(ExecutionLevel::AsInvoker)
        .supported_os(SupportedOS::Windows10..);
    embed_manifest(manifest).expect("unable to embed manifest file");
    println!("cargo:rerun-if-changed=build.rs");
}
