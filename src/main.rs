#![windows_subsystem = "windows"]

use std::mem::size_of;
use std::process::exit;

use windows::core::{w, Error, HSTRING};
use windows::Win32::{
    Foundation::{HWND, LPARAM, LRESULT, WPARAM},
    System::Diagnostics::Debug::OutputDebugStringW,
    System::LibraryLoader::GetModuleHandleW,
    UI::WindowsAndMessaging::*,
};

/// `WindowProc` callback function that processes messages sent to our window by
/// passing them all on to [`DefWindowProcW`].
unsafe extern "system" fn main_wnd_proc(
    hwnd: HWND,
    msg: u32,
    wparam: WPARAM,
    lparam: LPARAM,
) -> LRESULT {
    DefWindowProcW(hwnd, msg, wparam, lparam)
}

/// Creates a message-only window and sends an [`SC_MONITORPOWER`] system command
/// to it, to ask Windows to turn off all attached monitors immediately.
fn send_screenoff() -> windows::core::Result<()> {
    let class_name = w!("MainWClass");
    let window_name = w!("Screen Off");

    // Get the instance handle from GetModuleHandleW, as would be passed to WinMain.
    let h_instance = unsafe { GetModuleHandleW(None).unwrap() };

    // Register a basic window class for windows that use `main_wnd_proc` as their
    // message processor.
    let wnd_class = WNDCLASSEXW {
        cbSize: size_of::<WNDCLASSEXW>() as u32,
        lpfnWndProc: Some(main_wnd_proc),
        hInstance: h_instance.into(),
        lpszClassName: class_name,
        ..Default::default()
    };
    if unsafe { RegisterClassExW(&wnd_class) } == 0 {
        return Err(Error::from_win32());
    }

    // Create an invisible message-only window with `HWND_MESSAGE` as its parent.
    let hwnd = unsafe {
        CreateWindowExW(
            Default::default(),
            class_name,
            window_name,
            WINDOW_STYLE(0),
            CW_USEDEFAULT,
            CW_USEDEFAULT,
            CW_USEDEFAULT,
            CW_USEDEFAULT,
            HWND_MESSAGE,
            HMENU(0),
            h_instance,
            None,
        )
    };
    if hwnd == HWND(0) {
        return Err(Error::from_win32());
    }

    // Send the system command message requesting to turn the monitor power off,
    // with lparam set to 2.
    unsafe { SendNotifyMessageW(hwnd, WM_SYSCOMMAND, WPARAM(SC_MONITORPOWER as _), LPARAM(2)) }
}

/// Report errors to an attached debugger, if any.
fn main() {
    if let Err(e) = send_screenoff() {
        unsafe {
            OutputDebugStringW(&HSTRING::from(format!("Error: {e:?}\n")));
        }
        exit(1)
    }
}
